plugins {
    groovy
    id("com.github.ben-manes.versions") version "0.42.0"
    id("com.github.hierynomus.license") version "0.16.1"
    id("com.github.johnrengelman.shadow") version "7.1.2"
}

java {
    toolchain.languageVersion.set(JavaLanguageVersion.of(JavaVersion.VERSION_17.majorVersion))
    withSourcesJar()
}

license {
    header = file("NOTICE")
    strictCheck = true
}

repositories {
    mavenCentral()
}

dependencies {
    @Suppress("LocalVariableName") val `groovy-version`: String by project

    implementation(group = "com.formdev", name = "flatlaf", version = "2.3")
    implementation(group = "org.apache.groovy", name = "groovy", version = `groovy-version`)
    implementation(group = "org.apache.groovy", name = "groovy-ginq", version = `groovy-version`)
    implementation(group = "org.apache.groovy", name = "groovy-nio", version = `groovy-version`)
    implementation(group = "org.apache.groovy", name = "groovy-swing", version = `groovy-version`)
    implementation(group = "net.boeckling", name = "crc-64", version = "1.0.0")
}

tasks {
    build.get().dependsOn(shadowJar)

    jar {
        manifest {
            attributes("Main-Class" to "net.thesilkminer.tooling.hash.HashChecker")
        }
    }

    withType<GroovyCompile> {
        groovyOptions.optimizationOptions?.put("indy", true)
    }

    withType<Wrapper> {
        gradleVersion = "7.3.3"
        distributionType = Wrapper.DistributionType.ALL
    }
}
