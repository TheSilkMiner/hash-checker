/*
 * Hash Checker: simple hash checking in a box
 * Copyright (C) 2023  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.hash

import groovy.transform.PackageScope
import net.boeckling.crc.CRC64

import java.util.zip.Checksum

@PackageScope
final class Crc64ChecksumAdapter implements Checksum {
    private CRC64 crc

    Crc64ChecksumAdapter() {
        this.reset()
    }

    @Override
    void update(final int b) {
        this.crc.update([b as byte] as byte[], 1)
    }

    @Override
    void update(final byte[] b, final int off, final int len) {
        final byte[] newByte = new byte[len]
        System.arraycopy(b, off, newByte, 0, len)
        this.crc.update(newByte, newByte.length)
    }

    @Override
    long getValue() {
        this.crc.value
    }

    @Override
    void reset() {
        this.crc = new CRC64()
    }
}
