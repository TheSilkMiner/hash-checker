/*
 * Hash Checker: simple hash checking in a box
 * Copyright (C) 2023  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.hash

import java.security.Security

final class HashChecker {
    private HashChecker() {}

    static void main(final String... args) {
        final digests = Security.getAlgorithms('MessageDigest')
        final checksums = ['Size', 'CRC-32', 'CRC-32C', 'CRC-64'].toSet()
        final lookup = new ServiceLookup(digests, checksums)
        final model = new DataModel()
        final controller = new UiController(model, lookup)
        final ui = new UiBuilder(lookup.all, model, controller)
        ui.launch()
    }
}
