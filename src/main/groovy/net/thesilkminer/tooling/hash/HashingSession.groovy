/*
 * Hash Checker: simple hash checking in a box
 * Copyright (C) 2023  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.hash

import groovy.transform.MapConstructor
import groovy.transform.PackageScope

@MapConstructor(includeFields = true)
@PackageScope
final class HashingSession {
    private final String algorithm
    private final Closure<Void> updater
    private final Closure<String> digester

    void append(final byte[] contents) {
        this.updater(contents)
    }

    HashResult hash() {
        final hash = this.digester()
        new HashResult(algorithm: this.algorithm, hash: hash, match: null)
    }
}
