/*
 * Hash Checker: simple hash checking in a box
 * Copyright (C) 2023  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.hash

import groovy.transform.PackageScope

import java.security.MessageDigest
import java.util.zip.CRC32
import java.util.zip.CRC32C
import java.util.zip.Checksum

@PackageScope
final class ServiceLookup {
    private final Set<String> digests
    private final Set<String> checksums

    ServiceLookup(final Set<String> digests, final Set<String> checksums) {
        this.digests = new HashSet<>(digests)
        this.checksums = new HashSet<>(checksums)
    }

    Set<String> getAll() {
        this.digests + this.checksums
    }

    HashingSession hashSession(final String algorithm) {
        if (algorithm in this.checksums) {
            checksum(algorithm)
        } else if (algorithm in this.digests) {
            digest(algorithm)
        } else {
            throw new IllegalStateException("Unknown algorithm $algorithm")
        }
    }

    private static HashingSession digest(final String algorithm) {
        final digest = MessageDigest.getInstance(algorithm)
        new HashingSession(
                algorithm: algorithm,
                updater: { content -> digest.update(content) },
                digester: { toHex(digest.digest()) }
        )
    }

    private static HashingSession checksum(final String algorithm) {
        final checksumProvider = switch (algorithm) {
            case 'CRC-32' -> new CRC32()
            case 'CRC-32C' -> new CRC32C()
            case 'CRC-64' -> new Crc64ChecksumAdapter()
            case 'Size' -> new SizeChecksumAdapter()
            case String -> throw new IllegalStateException("Invalid Checksum algorithm $algorithm: did you forget to add the mapping?")
        }
        return checksum(algorithm, checksumProvider)
    }

    private static HashingSession checksum(final String algorithm, final Checksum checksum) {
        final base = algorithm == 'Size'? 10 : 16
        new HashingSession(
                algorithm: algorithm,
                updater: { content -> checksum.update(content) },
                digester: { Long.toUnsignedString(checksum.value, base).toUpperCase(Locale.ENGLISH) }
        )
    }

    private static String toHex(final byte[] hex) {
        final builder = new StringBuilder()
        for (final byte b : hex) {
            builder.append(toHex((b >> 4) & 0xF))
            builder.append(toHex(b & 0xF))
        }
        builder.toString().toUpperCase(Locale.ENGLISH)
    }

    private static String toHex(final int hex) {
        Integer.toString(hex, 16)
    }
}
