/*
 * Hash Checker: simple hash checking in a box
 * Copyright (C) 2023  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.hash

import groovy.transform.PackageScope

import java.util.zip.Checksum

@PackageScope
final class SizeChecksumAdapter implements Checksum {
    private long length

    SizeChecksumAdapter() {
        this.reset()
    }

    @Override
    void update(int b) {
        this.length += 1
    }

    @Override
    void update(final byte[] b, final int off, final int len) {
        this.length += len - off
    }

    @Override
    long getValue() {
        this.length
    }

    @Override
    void reset() {
        this.length = 0L
    }
}
