/*
 * Hash Checker: simple hash checking in a box
 * Copyright (C) 2023  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.hash

import com.formdev.flatlaf.FlatDarculaLaf
import groovy.swing.SwingBuilder
import groovy.transform.PackageScope

import javax.swing.BoxLayout
import javax.swing.WindowConstants
import javax.swing.event.DocumentEvent
import javax.swing.event.DocumentListener
import java.awt.BorderLayout
import java.nio.file.Path

@PackageScope
final class UiBuilder {
    private final SwingBuilder swing
    private final Set<String> knownAlgorithms
    private final DataModel model
    private final UiController controller

    @PackageScope
    UiBuilder(final Set<String> knownAlgorithms, final DataModel model, final UiController controller) {
        this.swing = new SwingBuilder()
        this.knownAlgorithms = knownAlgorithms
        this.model = model
        this.controller = controller
    }

    void launch() {
        FlatDarculaLaf.setup()

        this.swing.edt {
            final frame = frame(title: 'Hash Checker', defaultCloseOperation: WindowConstants.EXIT_ON_CLOSE, resizable: false) {
                borderLayout()
                [BorderLayout.NORTH, BorderLayout.SOUTH, BorderLayout.EAST, BorderLayout.WEST].each { // Borders
                    panel(constraints: it, preferredSize: [10, 10])
                }
                panel(constraints: BorderLayout.CENTER) { // Content Pane
                    final allCheckFields = [:]

                    borderLayout()
                    panel(constraints: BorderLayout.NORTH) { // File selector
                        boxLayout(axis: BoxLayout.LINE_AXIS)
                        hglue()
                        label(text: 'File: ')
                        final filePath = textField(preferredSize: [300, 20], text: bind(source: this.model, sourceProperty: 'targetFile', converter: UiBuilder.&filePath))
                        filePath.addActionListener { this.controller.chooseTargetFile(allCheckFields, filePath, filePath.text) }
                        button(text: '...', actionPerformed: { this.controller.chooseTargetFile(allCheckFields, filePath, null) })
                        button(
                                text: 'x',
                                actionPerformed: { this.controller.removeTargetFile(allCheckFields, filePath) },
                                enabled: bind(source: this.model, sourceProperty: 'targetFile', converter: UiBuilder.&enabled)
                        )
                        hglue()
                    }

                    panel(constraints: BorderLayout.CENTER) { // All algorithms panels
                        boxLayout(axis: BoxLayout.Y_AXIS)
                        this.knownAlgorithms.sort(UiBuilder.&sortWithBias).each { algorithm ->
                            def field = null

                            panel() { // Algorithm panel
                                boxLayout(axis: BoxLayout.LINE_AXIS)
                                label(text: "$algorithm: ")
                                panel() { // Hash and Check areas
                                    boxLayout(axis: BoxLayout.Y_AXIS)
                                    textField(preferredSize: [450, 20], text: bind(source: this.model, sourceProperty: 'results', converter: { v -> result(v, algorithm) }), editable: false)
                                    field = textField(preferredSize: [450, 20], enabled: bind(source: this.model, sourceProperty: 'targetFile', converter: UiBuilder.&enabled))
                                    field.document.addDocumentListener(new DocumentListener() {
                                        def background = field.background

                                        @Override
                                        void insertUpdate(final DocumentEvent e) {
                                            UiBuilder.this.controller.updateCheck(algorithm, field.text, field, this.background)
                                        }

                                        @Override
                                        void removeUpdate(final DocumentEvent e) {
                                            UiBuilder.this.controller.updateCheck(algorithm, field.text, field, this.background)
                                        }

                                        @Override
                                        void changedUpdate(final DocumentEvent e) {
                                            UiBuilder.this.controller.updateCheck(algorithm, field.text, field, this.background)
                                        }
                                    })
                                }
                                panel() { // Hash and Check buttons
                                    boxLayout(axis: BoxLayout.Y_AXIS)
                                    button(
                                            text: 'Compute',
                                            actionPerformed: { this.controller.computeHashes([algorithm]) },
                                            enabled: bind(source: this.model, sourceProperty: 'targetFile', converter: UiBuilder.&enabled)
                                    )
                                    button(
                                            text: 'Check',
                                            actionPerformed: { checkHash(this.controller, algorithm, { -> allCheckFields }) },
                                            enabled: bind(source: this.model, sourceProperty: 'targetFile', converter: UiBuilder.&enabled)
                                    )
                                }
                            }

                            allCheckFields.put(algorithm, [field, field.background])
                        }
                    }

                    panel(constraints: BorderLayout.SOUTH) { // Full action buttons
                        boxLayout(axis: BoxLayout.LINE_AXIS)
                        panel() {
                            boxLayout(axis: BoxLayout.LINE_AXIS)
                            button(
                                    text: 'Compute All',
                                    actionPerformed: { this.controller.computeHashes(this.knownAlgorithms) },
                                    enabled: bind(source: this.model, sourceProperty: 'targetFile', converter: UiBuilder.&enabled)
                            )
                            button(
                                    text: 'Check All',
                                    actionPerformed: { this.controller.checkHashes(this.knownAlgorithms, allCheckFields) },
                                    enabled: bind(source: this.model, sourceProperty: 'targetFile', converter: UiBuilder.&enabled)
                            )
                            button(
                                    text: 'Check Specified',
                                    actionPerformed: { this.controller.checkHashes(this.knownAlgorithms, allCheckFields, true) },
                                    enabled: bind(source: this.model, sourceProperty: 'targetFile', converter: UiBuilder.&enabled)
                            )
                        }
                        hglue()
                        button(text: 'Exit', actionPerformed: { System.exit(0) })
                    }
                }
            }
            frame.pack()
            frame.locationRelativeTo = null
            this.controller.frame = frame
            frame.visible = true
        }
    }

    private static int sortWithBias(final String a, final String b) {
        if (a == 'Size') {
            if (b == 'Size') {
                return 0
            }
            return -1
        }
        if (b == 'Size') {
            return 1
        }
        a <=> b
    }

    private static String filePath(final Path path) {
        path?.toAbsolutePath()?.normalize()?.toString() ?: ''
    }

    private static boolean enabled(final Path path) {
        path
    }

    private static String result(final Map<String, HashResult> results, final String algorithm) {
        results[algorithm]?.hash ?: ''
    }

    private static void checkHash(final UiController controller, final String algorithm, final Closure<Map<String, List<?>>> allFields) {
        controller.checkHashes([algorithm], allFields())
    }
}
