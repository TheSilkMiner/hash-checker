/*
 * Hash Checker: simple hash checking in a box
 * Copyright (C) 2023  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.hash

import groovy.transform.PackageScope
import groovy.transform.TailRecursive

import javax.swing.JFileChooser
import javax.swing.JTextField
import java.awt.Color
import java.awt.Component
import java.nio.file.Files
import java.nio.file.Paths

@PackageScope
final class UiController {
    private final DataModel model
    private final ServiceLookup lookup
    Component frame // Unfortunately stateful

    UiController(final DataModel model, final ServiceLookup lookup) {
        this.model = model
        this.lookup = lookup
    }

    void chooseTargetFile(final Map<String, List<?>> allFields, final Component file, final String choice) {
        final potentialPath = choice? Paths.get(choice) : {
            final cd = this.model.targetFile ?: Paths.get('.')
            final chooser = new JFileChooser(cd.toFile())
            final answer = chooser.showOpenDialog(this.frame)
            if (answer == JFileChooser.APPROVE_OPTION) {
                chooser.selectedFile.toPath()
            } else {
                null
            }
        }()
        if (!potentialPath) {
            return
        }
        final path = potentialPath.normalize().toAbsolutePath()
        if (Files.notExists(path)) {
            this.removeTargetFile(allFields, file)
            return
        }

        this.model.targetFile = path
        this.model.results = [:]
        allFields.values().each { it[0].background = it[1] }
    }

    void removeTargetFile(final Map<String, List<?>> allFields, final Component file) {
        this.model.targetFile = null
        this.model.results = [:]
        this.model.checks = [:]
        allFields.values().each {
            it[0].background = it[1]
            it[0].text = ''
        }
        file.requestFocus()
    }

    void computeHashes(final Collection<String> algorithms) {
        this.model.results = this.doComputeHashes(algorithms)
    }

    void checkHashes(final Collection<String> algorithms, final Map<String, List<?>> allFields, final boolean onlySpecific = false) {
        final targetAlgorithms = onlySpecific? algorithms.findAll { this.model.checks[it] != null } : algorithms
        final map = this.doComputeHashes(targetAlgorithms)
        final newMap = [:] as Map<String, HashResult>

        map.forEach { algorithm, result ->
            if (algorithm in targetAlgorithms) {
                final check = this.model.checks[algorithm]
                final match = check? doHashCheck(check, result.hash) : null
                newMap.put(algorithm, new HashResult(result.algorithm, result.hash, match))
            } else {
                newMap.put(algorithm, result)
            }
        }

        allFields.forEach {algorithm, data ->
            if (algorithm in targetAlgorithms) {
                final field = data[0] as JTextField
                final result = newMap[algorithm] as HashResult
                if (result.match != null) {
                    final color = result.match? new Color(0x15, 0x52, 0x21) : new Color(0x53, 0x2B, 0x2E)
                    field.background = color
                } else {
                    field.background = new Color(0x52, 0x50, 0x3A)
                }
            }
        }

        this.model.results = newMap
    }

    void updateCheck(final String algorithm, final String check, final JTextField field, final Color color) {
        final map = new LinkedHashMap<>(this.model.checks)
        map[algorithm] = check
        this.model.checks = map
        field.background = color
    }

    private LinkedHashMap doComputeHashes(final Collection<String> algorithms) {
        final map = new LinkedHashMap<>(this.model.results)
        this.model.targetFile.withInputStream {
            final sessions = algorithms.collect { alg -> this.lookup.hashSession(alg) }

            final bufferLength = 4096 // We read in increments of 4096 bytes
            final buffer = new byte[bufferLength]
            while (true) {
                final bytesRead = it.read(buffer)
                final resizedBuffer = resizeBuffer(buffer, bufferLength, bytesRead)
                if (!resizedBuffer) break
                sessions.each { it.append(resizedBuffer) }
            }

            sessions.each {
                final result = it.hash()
                map[result.algorithm] = result
            }
        }
        map
    }

    private static byte[] resizeBuffer(final byte[] buffer, final int bufLength, final int read) {
        if (read < 0) {
            return null
        }
        if (read == bufLength) {
            return buffer
        }
        final byte[] newBuffer = new byte[read]
        System.arraycopy(buffer, 0, newBuffer, 0, read)
        newBuffer
    }

    private static boolean doHashCheck(final String check, final String expected) {
        final immediate = check.equalsIgnoreCase(expected)
        immediate? immediate : stripZeros(check).equalsIgnoreCase(stripZeros(expected))
    }

    @TailRecursive
    private static String stripZeros(final String str) {
        !str.startsWith('0')? str : stripZeros(str.substring(1))
    }
}
